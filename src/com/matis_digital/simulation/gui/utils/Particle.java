package com.matis_digital.simulation.gui.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.LinkedList;

import com.matis_digital.simulation.Constants;
import com.matis_digital.simulation.Constants.BOUNDS;
import com.matis_digital.simulation.gui.MainWindow;

public class Particle
{
    private class Point
    {
        private transient final int x;
        private transient final int y;
        private transient final int z;

        public Point(double x, double y, double z)
        {
            this.x = (int) Math.round(x);
            this.y = (int) Math.round(y);
            this.z = (int) Math.round(z);
        }
    }

    private static final long                 serialVersionUID = 9060111474242640596L;
    private static final double               vmin             = 1e-20;
    private transient Color                   c;
    private transient final int               _id;
    public transient double                   x, y;                                   // location
    private transient double                  z;
    private transient boolean                 hit;
    private transient double                  m;
    private transient double                  vx, vy;
    private transient final LinkedList<Point> _trace;
    private transient final double            d;
    private transient boolean                 delete           = false;
    private transient final MainWindow        _main;

    public boolean isDelete()
    {
        return delete;
    }

    public Particle(final MainWindow mainWindow, final int id, final Color color, final double px, final double py, final double mass, final double radius, final double sx, final double sy)
    {
        _main = mainWindow;
        _id = id;
        d = mass / (Math.PI * radius * radius);
        z = radius;
        m = mass;
        c = color;
        x = px;
        y = py;
        vx = sx;
        vy = sy;
        _trace = new LinkedList<Point>();
    }

    @Override
    public String toString()
    {
        return String.format("%d %.1f %.1f %.1f %.1f %.1f", _id, x, y, z, m, d);
    }

    public Color getColor()
    {
        return c;
    }

    public int interact(Particle b)
    {
        double p = b.x - x;
        double q = b.y - y;
        if (Constants.BOUND == BOUNDS.WRAP)
        { // wrap around, use shortest distance
            if (p > _main.getDisplayWidth() / 2)
            {
                p -= _main.getDisplayWidth();
            }
            else if (p < -_main.getDisplayWidth() / 2)
            {
                p += _main.getDisplayWidth();
            }
            if (q > _main.getDisplayHeight() / 2)
            {
                q -= _main.getDisplayHeight();
            }
            else if (q < -_main.getDisplayHeight() / 2)
            {
                q += _main.getDisplayHeight();
            }
        }
        double h2 = p * p + q * q;
        if (Constants.COLLISION)
        { // collisions enabled
            double h = Math.sqrt(h2);
            if (h < z + b.z)
            { // HIT
                hit = b.hit = true;
                if (Constants.MUSH)
                {
                    int toReturn = 0;
                    // mush together
                    /* Delete lightest particle */
                    if (m < b.m)
                    {
                        toReturn = 1;
                    }
                    else
                    {
                        toReturn = 2;
                    }
                    if (b.m + m != 0)
                    {
                        double t = b.m / (b.m + m);
                        x += p * t;
                        y += q * t;
                        vx += (b.vx - vx) * t;
                        vy += (b.vy - vy) * t;
                        if (x > _main.getDisplayWidth())
                        {
                            x -= _main.getDisplayWidth();
                        }
                        if (x < _main.getDisplayX())
                        {
                            x += _main.getDisplayWidth();
                        }
                        if (y > _main.getDisplayHeight())
                        {
                            y -= _main.getDisplayHeight();
                        }
                        if (y < _main.getDisplayY())
                        {
                            y += _main.getDisplayHeight();
                        }
                    }
                    if (toReturn == 1)
                    {
                        b.m += m;
                        delete = true;
                    }
                    else if (toReturn == 2)
                    {
                        m += b.m;
                        b.delete = true;
                    }
                    z = Math.sqrt((m / d) / Math.PI);
                    return toReturn; // delete b
                }
                else if (h > 1e-10)
                {
                    // Compute the elastic collision of two balls.
                    // The math involved here is not for the faint of heart!

                    double v1, v2, r1, r2, s, t, v;
                    p /= h;
                    q /= h; // normalized impact direction
                    v1 = vx * p + vy * q;
                    v2 = b.vx * p + b.vy * q; // impact velocity
                    r1 = vx * q - vy * p;
                    r2 = b.vx * q - b.vy * p; // remainder velocity
                    if (v1 < v2) return 0;
                    s = m + b.m; // total mass
                    if (s == 0) return 0;

                    t = (v1 * m + v2 * b.m) / s;
                    v = t + Constants.RESTITUTION * (v2 - v1) * b.m / s;
                    vx = v * p + r1 * q;
                    vy = v * q - r1 * p;
                    v = t + Constants.RESTITUTION * (v1 - v2) * m / s;
                    b.vx = v * p + r2 * q;
                    b.vy = v * q - r2 * p;
                }
            }
        }
        if (Constants.MASS_GRAVITY != 0 && h2 > 1e-10 && !hit && !b.hit)
        { // gravity is enabled
            double dv;
            dv = Constants.MASS_GRAVITY * b.m / h2;
            vx += dv * p;
            vy += dv * q;
            dv = Constants.MASS_GRAVITY * m / h2;
            b.vx -= dv * p;
            b.vy -= dv * q;
        }
        return 0;
    }

    public void move()
    {
        x += vx;
        y += vy;
        if (Constants.BOUND != BOUNDS.NONE)
        {
            /* The particles should react to the screen boundary */
            if (x + z > _main.getDisplayWidth())
            {
                if (Constants.BOUND == BOUNDS.WRAP)
                {
                    x -= _main.getDisplayWidth();
                }
                else if (Constants.BOUND == BOUNDS.BOUNCE)
                {
                    if (vx > 0)
                    {
                        vx *= Constants.RESTITUTION; // restitution
                    }
                    vx = -Math.abs(vx) - vmin; // reverse velocity
                    hit = true;
                    // Check if location is completely off screen
                    if (x - z > _main.getDisplayWidth())
                    {
                        x = _main.getDisplayWidth() + z;
                    }
                }
            }
            if (x - z < _main.getDisplayX())
            {
                if (Constants.BOUND == BOUNDS.WRAP)
                {
                    x += _main.getDisplayWidth();
                }
                else if (Constants.BOUND == BOUNDS.BOUNCE)
                {
                    if (vx < 0)
                    {
                        vx *= Constants.RESTITUTION;
                    }
                    vx = Math.abs(vx) + vmin;
                    hit = true;
                    if (x + z < _main.getDisplayX())
                    {
                        x = -z;
                    }
                }
            }

            if (y + z > _main.getDisplayHeight())
            {
                if (Constants.BOUND == BOUNDS.WRAP)
                {
                    y -= _main.getDisplayHeight();
                }
                else if (Constants.BOUND == BOUNDS.BOUNCE)
                {
                    if (vy > 0)
                    {
                        vy *= Constants.RESTITUTION;
                    }
                    vy = -Math.abs(vy) - vmin;
                    hit = true;
                    if (y - z > _main.getDisplayHeight())
                    {
                        y = _main.getDisplayHeight() + z;
                    }
                }
            }
            if (y - z < _main.getDisplayY())
            {
                if (Constants.BOUND == BOUNDS.WRAP)
                {
                    y += _main.getDisplayHeight();
                }
                else if (Constants.BOUND == BOUNDS.BOUNCE)
                {
                    if (vy < 0)
                    {
                        vy *= Constants.RESTITUTION;
                    }
                    vy = Math.abs(vy) + vmin;
                    hit = true;
                    if (y + z < 0)
                    {
                        y = -z;
                    }
                }
            }
        }
        if (Constants.FRICTION > 0 && m != 0)
        { // viscosity
            double t = 100 / (100 + Constants.FRICTION * Constants.hypot(vx, vy) * z * z / m);
            vx *= t;
            vy *= t;
        }
        if (hit == false)
        {
            vy += Constants.GRAVITY; // if not hit, exert gravity
        }
        hit = false; // reset flag
        addTrace();
    }

    private void addTrace()
    {
        synchronized (_trace)
        {
            final int size = _trace.size();
            final Point curr = new Point(x, y, z);
            if (size > 0)
            {
                int lX = _trace.getLast().x;
                int lY = _trace.getLast().y;
                int lZ = _trace.getLast().z;
                int cX = curr.x;
                int cY = curr.y;
                int cZ = curr.z;
                if (lX == cX && lY == cY && lZ == cZ)
                {
                    return;
                }
            }
            if (size >= Constants.TRACE_LENGTH)
            {
                _trace.removeFirst();
            }
            _trace.addLast(curr);
        }

    }

    public void draw(final Graphics2D g2, final boolean filled)
    {
        int rX = (int) (x - z);
        int rY = (int) (y - z);
        int rZ = (int) (z * 2);
        g2.setColor(c);
        if (filled == true)
        {
            g2.fillOval(rX, rY, rZ, rZ);
        }
        else
        {
            g2.drawOval(rX, rY, rZ, rZ);
        }

    }

    public void drawTrace(final Graphics2D g2)
    {
        g2.setColor(c);
        synchronized (_trace)
        {
            for (int idx = 0; idx < _trace.size(); idx++)
            {
                final Point trace = _trace.get(idx);
                g2.fillRect(trace.x, trace.y, 1, 1);
            }
        }
    }
}
