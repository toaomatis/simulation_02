package com.matis_digital.simulation;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

import com.matis_digital.simulation.gui.MainWindow;
import com.matis_digital.simulation.gui.utils.Particle;

public final class ParticleManager extends ArrayList<Particle>
{
    private static final long          serialVersionUID = -1255442560625161705L;
    private static ParticleManager     _instance;
    private transient final MainWindow _main;

    private ParticleManager(int size, final MainWindow mainWindow)
    {
        super();
        _main = mainWindow;
        if (size < 0)
        {
            final double particleSize = Math.PI * Math.pow(Constants.RADIUS, 2.0);
            final int windowSize = mainWindow.getDisplayWidth() * mainWindow.getDisplayHeight();
            final double particleCount = windowSize / particleSize;
            size = (int) Math.floor(particleCount * Constants.FILL_RATE);
        }
        fillArray(size);
    }

    public void animate()
    {
        for (int idx = 0; idx < size(); idx++)
        {
            final Particle particleA = get(idx);
            for (int nidx = idx + 1; nidx < size(); nidx++)
            {
                final Particle particleB = get(nidx);
                particleA.interact(particleB);
            }
        }
        for (int idx = 0; idx < size(); idx++)
        {
            final Particle particle = get(idx);
            particle.move();
        }
        if (Constants.MUSH == true)
        {
            for (int idx = 0; idx < size(); idx++)
            {
                final Particle particle = get(idx);
                if (particle.isDelete() == true)
                {
                    remove(idx);
                }
            }
        }
    }

    private void fillArray(final int size)
    {
        final Random rand = new Random(System.nanoTime());
        while (size() < size)
        {
            /* Will produce only bright / light colors: */
            final float r = (rand.nextFloat() * 0.5f) + 0.5f;
            final float g = (rand.nextFloat() * 0.5f) + 0.5f;
            final float b = (rand.nextFloat() * 0.5f) + 0.5f;
            final Color color = new Color(r, g, b);
            /* Set starting point */
            final double x = Math.floor((_main.getDisplayWidth() - Constants.RADIUS) * rand.nextDouble());
            final double y = Math.floor((_main.getDisplayHeight() - Constants.RADIUS) * rand.nextDouble());
            /* Generate physical properties */
            final double m = (rand.nextDouble() * 1.0f) + 1.0f;
            final double radius = (rand.nextDouble() * 1.0f) + 1.0f;
            final double sx = (rand.nextDouble() * Constants.SPEED) - (0.5f * Constants.SPEED);
            final double sy = (rand.nextDouble() * Constants.SPEED)- (0.5f * Constants.SPEED);
            /* Add particle to queue */
            final Particle particle = new Particle(_main, size(), color, x, y, m, radius, sx, sy);
            add(particle);
        }
        System.out.printf("Added %d particels \n", size());
    }

    public static ParticleManager getInstance(final int size, final MainWindow mainWindow)
    {
        if (_instance == null)
        {
            _instance = new ParticleManager(size, mainWindow);
        }
        return _instance;
    }

}
