package com.matis_digital.simulation;

public class Constants
{
    public static enum BOUNDS
    {
        WRAP, BOUNCE, NONE
    }

    public static final double  GRAVITY      = 0.0f;       /* 0,20,0,100 */
    public static final double  MASS_GRAVITY = 0.05f;      /* 0,20,10,100 */
    public static final double  RESTITUTION  = 0.85f;      /* 0,20,17,20 */
    public static final double  FRICTION     = 0.0f;       /* 0,20,0,20 */

    public static final double  RADIUS       = 1.0f;
    public static final double  FILL_RATE    = 0.01;
    public static final double  SPEED        = 0.0f;

    // public static final double DENSITY = 1.0f;

    public static final boolean COLLISION    = true;
    public static final boolean MUSH         = false;
    public static final int     TRACE_LENGTH = 100;
    public static final int     PARTICLES    = 1000;
    public static BOUNDS        BOUND        = BOUNDS.NONE;

    public static double hypot(double x, double y)
    {
        return Math.sqrt(x * x + y * y);
    }
}
