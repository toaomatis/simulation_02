package com.matis_digital.simulation;

import com.matis_digital.simulation.gui.MainWindow;

public class Simulation
{
    public static void main(String[] args)
    {
        final MainWindow mainWindow = new MainWindow();
        mainWindow.loop = new Thread(mainWindow);
        mainWindow.loop.start();
    }
}
